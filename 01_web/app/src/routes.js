function lazyLoad(component){
    return () => import(`@/${component}.vue`)
}

export default [
    {
        path: '/',
        name: 'home',
        meta: {requiresAuth: false},
        component: lazyLoad('views/home/index')
    },
    {
        path: '/security',
        name: 'security',
        meta: {requiresAuth: false},
        component: lazyLoad('views/security/index'),
        children: [
            {
                name: 'security.login',
                path: 'login',
                component: lazyLoad('components/security/login/index')
            },
            {
                name: 'security.register',
                path: 'register',
                component: lazyLoad('components/security/register/index')
            },
            {
                name: 'security.activate',
                path: 'activate/:token',
                component: lazyLoad('components/security/activate/index')
            },
            {
                name: 'security.forgot',
                path: 'password/forgot',
                component: lazyLoad('components/security/forgot/index')
            },
            {
                name: 'security.reset',
                path: 'password/reset/:token',
                component: lazyLoad('components/security/reset/index')
            }
        ]
    },
    {
        path: '/user',
        name: 'user',
        meta: {requiresAuth: true},
        component: lazyLoad('views/user/index'),
        children: [
            {
                name: 'user.dashboard',
                path: 'dashboard',
                component: lazyLoad('components/user/dashboard/index')
            },
            {
                name: 'user.profile',
                path: 'profile',
                component: lazyLoad('components/user/profile/index')
            },
            {
                name: 'user.account',
                path: 'account',
                component: lazyLoad('components/user/account/index')
            }

        ]
    }
]
