import { mapActions } from 'vuex'

export default {
    name: "activate",
    data() {
        return {
            token: null,
            success: null
        }
    },
    mounted() {
        this.token = this.$route.params.token;
        this.activate();
    },
    methods: {
        ...mapActions('security', ['activateAccount']),
        async activate() {
            let response = await this.activateAccount(this.token);
            if(response.status === 200) this.success = true;
        }
    }
}
