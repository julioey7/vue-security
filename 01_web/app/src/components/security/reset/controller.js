const { required, minLength, email, sameAs, requiredIf } =  require('vuelidate/lib/validators');
import {mapActions} from 'vuex'
export default {
    name: "reset",
    props: ['noToken'],
    data() {
        return {
            user: {
                token: null,
                old_password: '',
                password: '',
                password2: ''
            },
            submitted: false,
            valid: null
        }
    },
    validations: {
        user: {
            old_password: { required: requiredIf(function () {return this.noToken}), minLength: minLength(8) },
            password2: { required, sameAsPassword: sameAs("password") },
            password: {
                required, minLength: minLength(8),
                hasLetters(password) { return (/[a-z]/.test(password)) },
                hasNumbers(password) { return (/[0-9]/.test(password)) },
                hasSpecialChar(password) { return(/\W|_/.test(password)) }
            }
        }
    },
    computed: {

    },
    methods: {
        ...mapActions('security', ['resetPassword', 'validateToken', 'updatePassword']),
        async submit() {
            let response = null;
            if(!this.noToken) response = await this.resetPassword(this.user);
            else response = await this.updatePassword(this.user);
            if(response.status && response.status === 200) {
                this.submitted = true;
                this.user.old_password = '';
                this.user.password = '';
                this.user.password2 = '';
            }
        },
        async validate() {
            let response = await this.validateToken(this.user.token);
            if(response.status && response.status === 200) this.valid = true;
        }
    },
    created() {
        if(!this.noToken) {
            this.user.token = this.$route.params.token;
            this.validate()
        }
    }
}
