import {mapMutations, mapActions} from 'vuex';
const { required, minLength, email } =  require('vuelidate/lib/validators');

export default {
    name: "Login",
    data() {
        return {
            user: {
                username: '',
                password: '',
                device: {},
                location: {
                    lat: null,
                    long: null
                }
            },

        }
    },
    validations: {
        user: {
            username: {
                required, email
            },
            password: {
                required,
                minLength: minLength(8)
            }
        }
    },
    computed: {
        emailHasContent: function() {return this.user.username.length > 0},
        passHasContent: function() {return this.user.password.length > 0},
    },
    methods: {
        ...mapActions('security', ['login']),
        ...mapMutations('security', {setAuth: 'set'}),
        ...mapMutations('profile', {setUser: 'set'}),
        async submit() {
            let response = await this.login(this.user);
            if(response.status === 200) {
                this.setAuth(response.data.authorization);
                this.setUser(response.data);
                await this.$router.push('/user/dashboard');
            } else this.user.password = '';
        }
    },
    mounted() {
        this.user.device.device = window.navigator.platform;
        this.$getLocation({enableHighAccuracy: true}).then(coordinates => {
            this.user.location.lat = coordinates.lat;
            this.user.location.long = coordinates.lng;
        });
    }
}
