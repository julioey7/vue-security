import { mapActions } from 'vuex';
const { required, email } =  require('vuelidate/lib/validators');

export default {
    name: "forgot",
    data() {
        return {
            user: {
                email: ''
            },
            success: false
        }
    },
    validations: {
        user: {
            email: {
                required, email
            }
        }
    },
    computed: {
        emailHasContent: function() {return this.user.email.length > 0}
    },
    methods: {
        ...mapActions('security', ['forgotPassword']),
        async forgotPass() {
            // this.$Progress.start();
            let response = await this.forgotPassword(this.user);
            console.log(response);
            if(response.status === 200) {
                this.success = true;
            } else {
                this.user.email = '';
            }
        }
    }
}
