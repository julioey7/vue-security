const { required, minLength, email } =  require('vuelidate/lib/validators');
import {mapActions} from 'vuex'
export default {
    name: "register",
    data() {
        return {
            user: {
                name: '',
                email: '',
                password: ''
            },
            submitted: false
        }
    },
    validations: {
        user: {
            name: {
                required, minLength: minLength(4)
            },
            email: {
                required, email
            },
            password: {
                required, minLength: minLength(8),
                hasLetters(password) { return (/[a-z]/.test(password)) },
                hasNumbers(password) { return (/[0-9]/.test(password)) },
                hasSpecialChar(password) { return(/\W|_/.test(password)) }
            }
        }
    },
    computed: {
        nameHasContent: function() {return this.user.name.length > 0},
        emailHasContent: function() {return this.user.email.length > 0},
        passHasContent: function() {return this.user.password.length > 0}
    },
    methods: {
        ...mapActions('security', ['register']),
        async submit() {
            let response = await this.register(this.user);
            if(response.status && response.status === 201) this.submitted = true;
        }
    }
}
