export default {
    props: ['data', 'type'],
    methods: {
        cancel() {
            this.$emit('cancel', null);
        },
        confirm() {
            this.$emit('confirm', null);
        }
    }
}
