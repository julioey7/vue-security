import { mapState, mapMutations } from 'vuex';

export default {
    data() {
        return {
            currentRoute: null
        }
    },
    mounted() {
        this.currentRoute = this.$route.name;
    },
    computed: {
        ...mapState('profile', ['profile']),
        ...mapState('preferences', ['menu', 'lang'])
    },
    methods: {
        ...mapMutations('preferences', ['toggleMenu', 'setLang'])
    },
    watch: {
        '$route' (to, from) {
            this.currentRoute = to.name;
        }
    }
}
