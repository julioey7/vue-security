import { mapState } from "vuex";

export default {
    name: "profile",
    computed: {
        ...mapState('profile', ['profile'])
    }
}
