import {mapState, mapActions, mapMutations} from 'vuex'
const { required, minLength } =  require('vuelidate/lib/validators');
import appModal from '../../global/modal/index'
import resetForm from '../../security/reset/index'

export default {
    name: "account",
    computed: {
        ...mapState('security', ['user'])
    },
    methods: {
        ...mapActions('security', ['sessionList', 'deleteSession', 'deleteAccount']),
        ...mapMutations('security', ['clear']),
        isEven: function (number) {
            return number % 2 === 0
        },
        getSessions: async function () {
            let sessions = await this.sessionList();
            this.sessions = sessions.data;
        },
        openModal: function (session) {
            this.confirm = session;
        },
        closeModal: function () {
            this.confirm = null;
        },
        closeSession: async function(session) {
            let response = await this.deleteSession(session._id);
            if(response.status === 200) this.sessions.splice(this.sessions.indexOf(session), 1);
            this.confirm = null;
        },
        deleteMe: async function() {
            let response = await this.deleteAccount(this.account);
            if(response.status === 200) {
                this.clear();
                await this.$router.push('/security/login')
            }
        }
    },
    data() {
        return {
            sessions: null,
            confirm: null,
            account: {
                password: null
            }
        }
    },
    validations: {
        account: {
            password: {
                required,
                minLength: minLength(8)
            }
        }
    },
    created() {
        this.getSessions();
    },
    components: {
        appModal,
        resetForm
    }
}
