import moment from 'moment'
import Vue from 'vue'

Vue.filter('date', (value, format, timezone) => {
    format = format? format : 'MM/DD/YYY hh:mm a';
    timezone = timezone? timezone : '-06:00';
   if(value) return moment(String(value)).utcOffset(timezone).format(format);
});
