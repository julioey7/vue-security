import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import {faUserSecret, faUserAlt, faBars, faHome, faUserCircle, faAddressCard, faSignOutAlt, faCompass,
    faMedal, faDiceFour, faCoins, faTrashAlt, faUsers, faCopyright} from '@fortawesome/free-solid-svg-icons'
import {faFacebookF, faTwitter, faInstagram} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserSecret, faUserAlt, faBars, faHome, faUserCircle, faAddressCard, faSignOutAlt, faCompass, faFacebookF,
    faTwitter, faInstagram, faMedal, faDiceFour, faCoins, faTrashAlt, faUsers, faCopyright);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;
