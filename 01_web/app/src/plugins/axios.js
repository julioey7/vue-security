import Vue from 'vue';
import axios from 'axios';
import vue_axios from 'vue-axios';
import app from '../main'

axios.defaults.baseURL = process.env.VUE_APP_API_URL;

axios.interceptors.request.use( config => {
    config.headers['Accept-Language'] = app.$store.state.preferences.lang || 'es_MX';
    app.$Progress.start();
    return config;
});

axios.interceptors.response.use( response => {
   app.$Progress.finish();
   return response;
}, error => {
    app.$Progress.fail();
    console.error("Error in axios request >>>", error);
    app.$toasted.error(error.response.data,
        {
            duration: 5000,
            position: "bottom-right", action: {
                text: 'Close', onClick: (e, toastObject) => { toastObject.goAway(0)}
            }
        });
    if(error.response.status === 401) {
        app.$store.state.security.access_token = null;
        app.$router.push('/security/login');
    }
    return error;
});

Vue.prototype.$axiosCall = function (method, url, data, body, auth) {
    return Vue.axios({
        method: method,
        url: url,
        data: data,
        headers: {'Authorization': auth? app.$store.state.security.access_token : null}
    });
};

Vue.use(vue_axios, axios);
