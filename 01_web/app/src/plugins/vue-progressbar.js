import Vue from 'vue'
import VueProgressBar from 'vue-progressbar'

const options = {
    color: '#536DFE',
    failedColor: '#874b4b',
    thickness: '5px',
    transition: {
        speed: '0.5s',
        opacity: '0.4s',
        termination: 300
    },
    autoRevert: true,
    location: 'top',
    inverse: false
};

Vue.use(VueProgressBar, options);