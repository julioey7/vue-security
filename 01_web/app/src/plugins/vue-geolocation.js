import Vue from 'vue'
import geolocation from 'vue-browser-geolocation'

Vue.use(geolocation);