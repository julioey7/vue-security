require('./axios');
require('./vuelidate');
require('./fontawesome');
require('./vue-progressbar');
require('./vue-toasted');
require('./vue-geolocation');