import appHeader from '../../components/global/header/index';
export default {
    name: 'home',
    components: {
        appHeader
    },
    data() {
        return {
            instructions: 'points'
        }
    }
}
