import AppHeader from '../../components/global/header/index'
import {mapActions, mapState, mapMutations} from 'vuex'

export default {
    name: "user",
    components: {
        AppHeader
    },
    created() {
        this.getProfile();
    },
    computed: {
        ...mapState('profile', ['profile']),
        ...mapState('preferences', ['menu'])
    },
    methods: {
        ...mapActions('security', ['logout']),
        ...mapActions('profile', ['getProfile']),
        ...mapMutations('security', {clearToken: 'clear'}),
        ...mapMutations('profile', {clearProfile: 'clear'}),
        async signOut() {
            await this.logout();
            this.clearToken();
            this.clearProfile();
            await this.$router.push('/security/login')
        }
    }
}
