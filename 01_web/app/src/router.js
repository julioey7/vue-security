import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'
import store from './store'

Vue.use(Router, {
    linkActiveClass: 'active'
});

let router =  new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const authorization = store.state.security.access_token;

    if(requiresAuth && !authorization) return next('/security/login');
    if(authorization && to.name.includes('security')) return next('/user/dashboard');

    store.state.preferences.menu = false;

    next();
});


export default router;
