export function clear(state) {
    state.profile = null
}

export function set(state, profile) {
    state.profile = {};
    state.profile.name = profile.name;
    state.profile.email = profile.email;
    state.profile.role = profile.role;
    state.profile.image = profile.image || null;
}
