import app from '../../main';
export async function getProfile(context) {
    let response = await app.$axiosCall('GET', 'user/profile', null, null, true);
    if(response.status === 200) context.commit('set', response.data);
}
