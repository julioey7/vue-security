export function toggleMenu(state) {
    state.menu = !state.menu;
}

export function setLang(state, lang) {
    state.lang = lang;
}