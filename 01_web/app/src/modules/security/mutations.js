export function set(state, access_token) {
    state.access_token = access_token;
    state.is_logged = !! access_token;
}

export function clear(state) {
    state.access_token = null;
    state.is_logged = false;
}
