import Vue from 'vue';

export function login(context, user) {
        return Vue.axios({
            method: 'POST',
            url: '/security/login',
            data: user
        });
}

export function register(context, user) {
    return Vue.axios({
        method: 'POST',
        url: '/security/register',
        data: user
    });
}

export function forgotPassword(context, user) {
        return Vue.axios({
            method: "POST",
            url: '/security/password/forgot',
            data: user
        });
}

export function resetPassword(context, user) {
    return Vue.axios({
        method: "POST",
        url: `/security/password/reset/${user.token}`,
        data: user
    });
}

export function updatePassword(context, user) {
    return Vue.axios({
        method: "PUT",
        url: `/security/password/update`,
        headers: {Authorization: context.getters.accessToken},
        data: user
    });
}

export function validateToken(context, token) {
    return Vue.axios({
        method: "GET",
        url: `/security/validate/token/${token}`
    });
}

export function activateAccount(context, token) {
    return Vue.axios({
        method: "GET",
        url: `/security/activate/${token}`
    });
}

export function sessionList(context) {
    return Vue.axios({
        method: "GET",
        url: "/security/session/list",
        headers: {authorization: context.state.access_token}
    })
}

export function deleteSession(context, id) {
    let url = id? `/security/session/delete/${id}` : "/security/session/delete";
    return Vue.axios({
        method: "DELETE",
        url: url,
        headers: {authorization: context.state.access_token}
    })
}

export function logout(context) {
    return Vue.axios({
        method: "POST",
        url: '/security/logout',
        headers: {authorization: context.state.access_token}
    });
}

export function deleteAccount(context, account) {
    return Vue.axios({
        method: 'DELETE',
        url: '/security/delete/me',
        headers: {authorization: context.state.access_token},
        data: account
    })
}
