import Vue from 'vue'
import App from './components/app/index.vue'
import router from './router'
import store from './store'

require('./plugins');
require('./filters');
Vue.config.productionTip = false;

export default new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
