import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import security from './modules/security'
import preferences from './modules/preferences'
import profile from './modules/profile'


Vue.use(Vuex);

const localStorage = new VuexPersist({
  storage: window.localStorage,
  modules: ['security', 'preferences']
});

const sessionStorage = new VuexPersist({
  storage: window.sessionStorage,
  modules: ['profile']
});

export default new Vuex.Store({
  modules: {
    security,
    preferences,
    profile
  },
  plugins: [localStorage.plugin, sessionStorage.plugin]
})
