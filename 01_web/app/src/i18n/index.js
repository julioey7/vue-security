import Vue from 'vue'
import Vuei18n from 'vue-i18n'

import en_US from './en_US'
import es_MX from './es_MX'

Vue.use(Vuei18n);

const messages = {
    en_US,
    es_MX
};

export default new Vuei18n({
   locale: 'en_US',
   fallbackLocale: 'es_MX',
   messages
});