const express = require('express');
const mongoose = require('mongoose');
const http = require('http');
const socket = require('socket.io');

const app = express();
const bodyParser = require('body-parser');
const errorMiddleware = require('./server/middlewares/error');

// Generate server instance
let server = http.createServer(app);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// DB Connection
mongoose.connect(process.env.DB_URL, { useNewUrlParser: true }, (error, response) => {
    if (error) throw error;
});

app.use(express.static('node_modules'));
app.use(express.static('public'));

// Config headers
app.use((request, response, next) => {
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Accept, Access-Control-Allow-Request-Methods, Accept-Language, content-type, Access-Control-Allow-Origin');
    response.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    response.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// Routes
app.use('/security', require('./server/routes/security'));
app.use('/user', require('./server/routes/user'));

//Error middleware
app.use(errorMiddleware.basicErrorHandler);

// Socket
module.exports.io = socket(server);
// require('./server/sockets/security');

// Jobs

// Start application
server.listen(process.env.PORT, () => {
    require('./server/bootstrap/bootstrap');
    console.info(`Server started at port: ${ process.env.PORT }`);
});
