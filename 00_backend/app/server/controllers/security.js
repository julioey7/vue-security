const User = require('../models/user');
const Token = require('../models/token');
const mailService = require('../services/mail');
const AuthCode = require('../models/auth_code');

class securityController {
    static async register(request, response) {
        let json = request.body;
        if (!json.name || !json.email || !json.password) {
            let field = !json.name ? 'name' : !json.email ? 'email' : 'password';
            response.status(409).send(`com.app.request.error.${field}.required`);
            return
        }

        let user = await new User(json).save();
        let token = await new Token({
            type: "register_token",
            username: user.email,
            device: json.device? json.device.device || 'unknown' : 'unknown',
            os: json.device? json.device.os || 'unknown' : 'unknown',
            browser: json.device? json.device.browser || 'unknown' : 'unknown',
            latitude: json.location ? json.location.latitude || null : null,
            longitude: json.location ? json.location.longitude || null : null
        }).save();

        let params = {
            user: { email: user.email, name: user.name, url: `${request.get('origin')}/security/activate/${token.token}` },
            type: 'sign_up',
            locale: request.headers['accept-language'] || 'es_MX'
        };

        await mailService.sendEmail(params);
        response.status(201).send('com.app.request.success.user.created');
    }

    static async login(request, response) {
        let json = request.body;
        if (!json.username || !json.password) {
            let field = !json.username ? 'username' : 'password';
            return response.status(409).send('com.app.request.error.' + field + '.required');
        }

        let user = await User.findOne({ email: json.username.toLowerCase() });
        if (!user || !await user.validatePassword(json.password)) return response.status(401).send('com.app.login.error.wrong.credentials');
        if (!user.active) return response.status(403).send('com.app.login.error.inactive.account');

        let token = await new Token({
            type: user.two_step_auth ? 'pre_auth_token' : 'access_token',
            username: user.email,
            device: json.device? json.device.device || 'unknown' : 'unknown',
            os: json.device? json.device.os || 'unknown' : 'unknown',
            browser: json.device? json.device.browser || 'unknown' : 'unknown',
            latitude: json.location ? json.location.lat || null : null,
            longitude: json.location ? json.location.long || null : null
        }).save();

        if (token.type === 'pre_auth_token') {
            let authCode = await new AuthCode({ username: user.email }).save();
            let params = {
                user: { email: user.email, name: user.name, code: authCode.code },
                type: 'validate_auth',
                locale: request.headers['accept-language'] || 'es_MX'
            };
            await mailService.sendEmail(params);
        }

        response.status(200).send({
            name: user.name,
            email: user.email,
            image: user.image,
            authorization: "Bearer " + token.token,
            role: token.type === 'pre_auth_token'? 'PRE_AUTH' : user.role,
        });
    }

    static async activate(request, response) {
        if (!request.params.token) {
            response.status(409).send('com.app.request.error.token.required');
            return;
        }

        let activationToken = await Token.findOne({ token: request.params.token });
        if (!activationToken) return response.status(404).send('com.app.response.error.token.not.found');
        if (activationToken.type !== 'register_token') return response.status(409).send('com.app.response.error.token.type.invalid');
        let user = await User.findOne({ email: activationToken.username });
        await activationToken.remove();
        if (!user) return response.status(404).send('com.app.response.error.user.not.found');
        user.active = true;
        await user.save();

        response.status(200).send('com.app.response.success.account.activated');
    }

    static async forgotPassword(request, response) {
        if (!request.body.email) {
            response.status(409).send('com.app.request.error.email.required');
            return;
        }

        let user = await User.findOne({ email: request.body.email });
        if (!user) return response.status(404).send('com.app.response.error.user.not.found');
        let token = await new Token({ type: 'recover_password', username: request.body.email }).save();
        let params = {
            user: { email: user.email, name: user.name, url: `${request.get('origin')}/security/password/reset/${token.token}` },
            type: 'restore_password',
            locale: request.headers['accept-language'] || 'es_MX'
        };
        await mailService.sendEmail(params);
        response.status(200).send('com.app.request.success.recovery.email.sent');
    }

    static async resetPassword(request, response) {
        let json = request.body;
        if (!request.params.token || !json.password || !json.password2) {
            let field = !json.password ? 'password' : !json.password2 ? 'password2' : 'token';
            response.status(409).send('com.app.request.error.' + field + '.required');
            return;
        }

        if (json.password !== json.password2) {
            response.status(409).send('com.app.request.error.password.mismatch');
            return;
        }

        let token = await Token.findOne({ token: request.params.token });
        if (!token) return response.status(404).send('com.app.request.error.token.not.found');

        let user = await User.findOne({ email: token.username });
        if (!user) return response.status(404).send('com.app.request.error.user.not.found');
        user.password = json.password;
        await user.save();
        token.remove();
        response.status(200).send('com.app.response.success.password.updated');
    }

    static async updatePassword(request, response) {
        let json = request.body;
        if(!json.old_password || !json.password || !json.password2) {
            let field = !json.old_password? 'old_password' : !json.password? 'password': 'password2';
            return response.status(400).send(`com.app.request.error.${field}.required`);
        }

        let user = await User.findOne({_id: request.locals.user._id});
        if(!await user.validatePassword(json.old_password)) return response.status(400).send('com.app.request.error.password.mismatch');
        if(json.password !== json.password2) return response.status(400).send('com.app.request.error.new.password.mismatch');

        user.password = json.password;
        await user.save();
        response.status(200).send('com.app.request.success.password.updated');
    }

    static async validateToken(request, response) {
        if (!request.params.token) {
            response.status(409).send('com.app.request.error.email.required');
            return;
        }

        let token = await Token.findOne({ token: request.params.token });
        if (!token) return response.status(404).send('com.app.response.error.token.not.found');
        if (token.type !== 'recover_password') {
            response.status(409).send('com.app.response.error.token.type.invalid');
            return;
        }

        response.status(200).send('com.app.response.success.token.valid');
    }

    static async validateAuth(request, response) {
        if (request.locals.token.type !== 'pre_auth_token') return response.status(403).send('com.app.request.error.access.forbidden');
        if (!request.params.code) return response.status(409).send('com.app.request.error.code.required');

        console.log("Looking for authcode >>");

        let authCode = await AuthCode.findOne({ code: parseInt(request.params.code), username: request.locals.token.username });
        if (!authCode) return response.status(404).send('com.app.response.error.authentication.invalid');

        let token = await Token.findOne({ _id: request.locals.token._id });
        if (!token) return response.status(404).send('com.app.response.error.token.not.found');

        await authCode.remove();
        token.type = 'access_token';
        await token.save();

        response.status(200).send({
            role: request.locals.user.role,
            username: token.username,
            authorization: `Bearer ${token.token}`
        });
    }

    static async logout(request, response) {
        let token = Token.findOne({ _id: request.locals.token._id });
        if (!token) return response.status(404).send('com.app.response.error.token.not.found');
        await token.remove();
        response.status(200).send('com.app.response.success.user.logged.out');
    }

    static async sessions(request, response) {
        let user = await User.findOne({_id: request.locals.user._id});
        if(!user) return response.status(404).send('com.app.request.error.user.not.found');

        let sessions = await Token.find({username: user.email, token: {$ne: request.locals.token.token}}).select('-token');
        response.status(200).send(sessions);
    }

    static async removeSession(request, response) {
        await Token.deleteOne({_id: request.params.id});
        response.status(200).send('com.app.request.success.session.deleted');
    }

    static async removeSessions(request, response) {
        await Token.deleteMany({username: request.locals.user.email, token: { $ne: request.locals.token.token}});
        response.status(200).send('com.app.request.success.sessions.deleted');
    }

    static async deleteMe(request, response) {
        let user = await User.findOne({_id: request.locals.user._id});
        let json = request.body;
        if (!user || !await user.validatePassword(json.password)) return response.status(400).send('com.app.request.error.wrong.credentials');
        await User.findOneAndDelete({_id: request.locals.user._id});
        await Token.deleteMany({username: user.email});
        response.status(200).send('com.app.request.success.user.deleted');
    }
}

module.exports = securityController;
