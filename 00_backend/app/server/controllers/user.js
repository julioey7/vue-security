const User = require('../models/user');
const mailService = require('../services/mail');

class userController {
    static profile(request, response) {
        response.status(200).send(request.locals.user)
    }

    static async list(request, response) {

        let from = Number(request.query.offset) || 0;
        let limit = Number(request.query.limit) || 20;

        let query = request.query.query? request.query.query.replace(/a/g, '[a,á,à,ä]')
            .replace(/e/g, '[e,é,ë]')
            .replace(/i/g, '[i,í,ï]')
            .replace(/o/g, '[o,ó,ö,ò]')
            .replace(/u/g, '[u,ü,ú,ù]') : '';

        let users = await User.find({ name: new RegExp('^' + query, 'i') }).sort({name: 1})
            .skip(from)
            .limit(limit);

        let count = await User.count({ name: new RegExp('^' + query, 'i') }).exec();
        response.status(200).send({
            users: users,
            count: count
        });
    }

    static async update(request, response) {
        let user = await User.findOne({ _id: request.params.id || request.locals.user._id});
        if(!user) response.status(404).send('com.app.request.error.user.not.found');

        let json = request.body;

        if(json.active !== undefined) user.active = json.active;
        if(json.role && (json.role === 'ROLE_ADMIN' || json.role === 'ROLE_USER')) user.role = json.role;
        if(json.name) user.name = json.name;
        if(json.phone) user.phone = json.phone;

        if(json.operation && json.amount) {
            if(json.operation === 'remove' && json.amount > user.points) return response.status(400).send('com.app.request.error.amount.overpass.user.points');
            user.points = json.operation === 'add'? user.points + json.amount : user.points - json.amount;

            await mailService.sendEmail({
                user: user,
                type: json.operation === 'add'? 'add_points_response' : 'remove_points_response',
                operation: json.operation,
                amount: json.amount,
                locale: request.headers['accept-language'] || 'es_MX'
            });
        }
        await user.save();
        response.status(200).send('com.app.request.success.user.updated');
    }

    static async get(request, response) {
        let user = await User.findOne({_id: request.params.id});
        if(!user) return response.status(404).send('com.app.request.error.user.not.found');

        response.status(200).send(user);
    }
}

module.exports = userController;
