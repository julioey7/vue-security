const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

function readHTML(path, callback) {
    fs.readFile(path, { encoding: 'utf-8' }, (error, html) => {
        if (error) {
            callback(error);
            return;
        }

        callback(null, html);
    });
}

async function sendEmail(params) {
    if (!params.type || !params.user) {
        return new Error('Missing params in mail service');
    }

    let templateUrl;
    let subject;

    if (params.type === 'contact') {
        templateUrl = path.join(__dirname, '../', 'templates/' + params.locale, '/_contact.html');
        subject = params.locale === 'en_US' ? "Contact | Security vue" : "Contacto | Security vue";
    } else if (params.type === 'sign_up') {
        templateUrl = path.join(__dirname, '../', 'templates/' + params.locale, '/_register.html');
        subject = params.locale === 'en_US' ? "New account | Security vue" : "Nueva cuenta | Security vue";
    } else if (params.type === 'restore_password') {
        templateUrl = path.join(__dirname, '../', 'templates/' + params.locale, '/_forgot_password.html');
        subject = params.locale === 'en_US' ? "Restore password | Security vue" : "Restaurar contraseña | Security vue";
    } else if (params.type === 'validate_auth') {
        templateUrl = path.join(__dirname, '../', 'templates/' + params.locale, '/_auth.html');
        subject = params.locale === 'en_US' ? "Validate session | Security vue" : "Validar sesión | Security vue";
    } else {
        templateUrl = null;
        subject = null;
    }

    if (!templateUrl || !subject) {
        return new Error('Missing template or subject in mail service');
    }

    let html = fs.readFileSync(templateUrl, 'utf8');

    let mailing = {
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            type: 'Oauth2',
            user: 'appmailingizy@gmail.com',
            clientId: '35761111583-tqs6u11pas8bmscbt3auf627dblgkqfn.apps.googleusercontent.com',
            clientSecret: 'TwGamlbb7eQQ9ZHZ4DqBc6Rz',
            refreshToken: '1/JEUapSqtqYjZM7fanVEaJ2HCEDH-5fxdZRQqZ1LYBZo',
            accessToken: 'ya29.GlvDBptZ4Y0U7-xn9EB6J4oxbeVfQWvvTBCXmzDv10Deqqaw9J7FkHSgTN7mPOrc5TXQzasJsNQbVpub-4q98FfI4V0Ft_x-jYAiUz6uu_0Z1Ye4BFij2P_CSJ4I',
            expires: '3600'
        }
    };

    let transporter = nodemailer.createTransport(mailing);

    let template = handlebars.compile(html);
    let replacements = {
        name: params.user.name,
        email: params.email || params.user.email,
        phone: params.phone || params.user.phone,
        message: params.message,
        url: params.user.url,
        code: params.user.code,
        password: params.user.password,
        amount: params.amount
    };

    let htmlToSend = template(replacements);

    let recipient = '';
    if(params.type === 'contact' || params.type === 'request_points' || params.type === 'redeem_points') {
        recipient = ['silver_splat@hotmail.com', 'dg.erickpadilla@gmail.com']
    } else recipient = params.user.email;

    let options = {
        from: mailing.auth.user,
        to: recipient,
        subject: subject,
        html: htmlToSend
    };

    transporter.sendMail(options, (error, info) => {
        if (error) {
            return error;
        }
        return info;
    });
}

module.exports = { sendEmail };