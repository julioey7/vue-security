const utilService = require('../services/util');
const Token = require('../models/token');
const User = require('../models/user');

function validateAuth(roles = []) {
    if(typeof roles === 'string') roles = [roles];

    return [
        async (request, response, next) => {
            if(!request.headers.authorization) return response.status(401).send('com.app.request.error.unauthorized');

            let token = await Token.findOne({token: utilService.getCleanToken(request.headers.authorization)});
            if(!token) return response.status(401).send('com.app.request.error.unauthorized');
            if(token.type === "PRE_AUTH" && !request.url.match(new RegExp('/validate/auth', 'i'))) return response.status(403).send('com.app.request.error.access.forbidden');

            let user = await User.findOne({email: token.username});
            if(!user) return response.status(401).send('com.app.request.error.unauthorized');
            if(!user.active) return response.status(403).send('com.app.request.error.user.blocked');

            if(roles.length && !roles.includes(user.role)) return response.status(403).send('com.app.request.error.access.forbidden');

            request.locals = {user, token: token};
            next();
        }
    ];
}

module.exports = validateAuth;