const express = require('express');
const router = express.Router();

const userController = require('../controllers/user');
const authorize = require('../middlewares/authentication');

router.get('/profile', authorize(), userController.profile);
router.get('/list', authorize('ROLE_ADMIN'), userController.list);
router.put('/update', authorize(), userController.update);
router.put('/update/:id', authorize('ROLE_ADMIN'), userController.update);
router.get('/get/:id', authorize('ROLE_ADMIN'), userController.get);

module.exports = router;
