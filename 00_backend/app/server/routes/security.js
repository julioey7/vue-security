const express = require('express');
const router = express.Router();
const util = require('../services/util');

const securityController = require('../controllers/security');
const authorize = require('../middlewares/authentication');

router.post('/register', util.wrapAsync(securityController.register));
router.post('/login', util.wrapAsync(securityController.login));
router.get('/activate/:token', util.wrapAsync(securityController.activate));
router.post('/password/forgot', util.wrapAsync(securityController.forgotPassword));
router.post('/password/reset/:token', util.wrapAsync(securityController.resetPassword));
router.put('/password/update', authorize(), util.wrapAsync(securityController.updatePassword));
router.get('/validate/auth/:code', authorize(), util.wrapAsync(securityController.validateAuth));
router.get('/validate/token/:token', util.wrapAsync(securityController.validateToken));
router.post('/logout', authorize(), util.wrapAsync(securityController.logout));
router.get('/session/list', authorize(), util.wrapAsync(securityController.sessions));
router.delete('/session/delete/:id', authorize(), util.wrapAsync(securityController.removeSession));
router.delete('/session/delete', authorize(), util.wrapAsync(securityController.removeSessions));
router.delete('/delete/me', authorize(), util.wrapAsync(securityController.deleteMe));

module.exports = router;
