const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const randToken = require('rand-token').generator();

let AuthenticationCode = Schema({
    code: {
        type: Number,
        required: true,
        unique: true,
        default: function() {
            return parseInt(randToken.generate(6, '0123456789'));
        }
    },
    username: {
        type: String,
        required: true
    }
}, {timestamps: true});

module.exports = mongoose.model('AuthenticationCode', AuthenticationCode);