
// USER MODEL
const mongoose = require('mongoose');
const argon2 = require('argon2');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let user = new Schema({
    name: {
        type: String,
        required: [true, 'com.app.user.error.name.required'],
    },
    email: {
        type: String,
        lowercase: true,
        required: [true, 'com.app.user.error.email.required'],
        match: [/\S+@\S+\.\S+/, 'com.app.user.error.email.invalid'],
        unique: true,
        index: true
    },
    password: {
        type: String,
        required: [true, 'com.app.user.error.password.required']
    },
    two_step_auth: {
        type: Boolean,
        required: true,
        default: false
    },
    active: {
        type: Boolean,
        default: false
    },
    role: {
        type: String,
        required: true,
        default: "ROLE_USER"
    }
}, {timestamps: true});

user.plugin(uniqueValidator, { message: 'com.app.user.error.{PATH}.duplicated' });
user.methods.toJSON = function() {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
};

user.pre('save', async function(next) {
    if(this.isModified('password') || this.isNew) {
        this.password = await argon2.hash(this.password);
        return next();
    }

    return next();
});

user.methods.validatePassword = async function(password) {
    return await argon2.verify(this.password, password)
};

module.exports = mongoose.model('User', user);