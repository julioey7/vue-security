const mongoose  = require('mongoose');
const randToken = require('rand-token').generator();
const Schema = mongoose.Schema;

let token = Schema({
    token: {
        type: String,
        required: true,
        default: function () {
            return randToken.generate(32);
        }
    },
    type: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    latitude: {
        type: String,
        required: false
    },
    longitude: {
        type: String,
        required: false
    },
    browser: {
        type: String,
        default: 'unknown'
    },
    os: {
        type: String,
        default: 'unknown'
    },
    device: {
        type: String,
        default: 'unknown'
    },
    session: {
        type: String,
        required: false,
        default: null
    }
}, {timestamps: true});

module.exports = mongoose.model('Token', token);